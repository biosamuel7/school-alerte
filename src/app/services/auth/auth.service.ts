import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { School } from 'src/app/models/school/school';
import { SchoolService } from '../school/school.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  schoolConnected$: Observable<School>;
  
  constructor(
    private afa: AngularFireAuth,
    private service: SchoolService
  ) { 
    this.schoolConnected$ = this.afa.authState.pipe(
      switchMap(user => {
        if(user){
          return this.service.getById(user.uid);
        } else {
          return of(null);
        }
      })
    )

  }

  signIn(email: string, password: string){
    return this.afa.signInWithEmailAndPassword(email, password)
  }

  logOut() {
    return this.afa.signOut()
  }

}
