import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map, switchMap } from 'rxjs/operators';
import { Member } from 'src/app/models/members/member';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class MemberService {

  constructor(
    private afs: AngularFirestore,
    private authService: AuthService
  ) { }

  get() {
    return this.authService.schoolConnected$
    .pipe(
      switchMap(user => this.afs.collection<Member>('members', ref => ref.where('schoolId','==',user.schoolId))
      .snapshotChanges()
      .pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data()
          const id = a.payload.doc.id
          return {id, ...data} as Member
        }))
      )
      )
    )
  }


  getById(id: string) {
    return this.authService.schoolConnected$
    .pipe(
      switchMap(user => this.afs.collection<Member>('members', ref => ref.where('schoolId','==',user.schoolId).where('number','==',id))
      .snapshotChanges()
      .pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data()
          const id = a.payload.doc.id
          return {id, ...data} as Member
        }))
      )
      )
    )
  }
  

  getByClassroom(ids: string[]) {
    return this.authService.schoolConnected$
    .pipe(
      switchMap(user => this.afs.collection<Member>('members', ref => ref.where('schoolId','==',user.schoolId).where('classroom','array-contains-any',ids))
      .snapshotChanges()
      .pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data()
          const id = a.payload.doc.id
          return {id, ...data} as Member
        }))
      )
      )
    )
  }

  add(data: Member){
    return this.afs.collection<Member>('members').add(data)
  }

}
