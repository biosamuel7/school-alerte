import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { ClassRoom } from 'src/app/models/classroom/classroom';

import * as firebase from "firebase/app";
import { AuthService } from '../auth/auth.service';


@Injectable({
  providedIn: 'root'
})
export class ClassroomService {

  private classroomCollection: AngularFirestoreCollection

  constructor(
    private afs: AngularFirestore,
    private authService: AuthService
  ) { 
    this.classroomCollection = afs.collection<ClassRoom>('classrooms')
  }

  get timestamp() {
    return firebase.firestore.FieldValue.serverTimestamp()
  }

  /**
   * Get all classrooms belong ton schoolConnected
   */
  get(): Observable<ClassRoom[]> {
    return this.authService.schoolConnected$.pipe(
      switchMap(user => this.afs.collection<ClassRoom>('classrooms', ref => ref.where('schoolId','==', user.schoolId))
      .snapshotChanges()
      .pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data()
          const id = a.payload.doc.id
          return {id, ...data} as ClassRoom
        }))
      ))
    )
  }

/**
 * add classroom on firestore database
 * @param data Observable<any>
 */
  add(data: any): Observable<any> {
    return this.authService.schoolConnected$.pipe(
      switchMap(user => of(this.classroomCollection.add({
        ...data,
        schoolId: user.schoolId,
        createdAt: this.timestamp,
        updatedAt: this.timestamp
      } as ClassRoom)))
    )
  }
/**
   * Get all classrooms belong ton schoolConnected
   */
  getBySchool(schoolId: string): Observable<ClassRoom[]> {
    return this.afs.collection<ClassRoom>('classrooms', ref => ref.where('schoolId','==', schoolId))
    .snapshotChanges(['added'])
    .pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data()
        const id = a.payload.doc.id
        return {id, ...data} as ClassRoom
      }))
    )
  }

}
