import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import { of } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import { Message } from 'src/app/models/message/message';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(
    private afs: AngularFirestore,
    private authService: AuthService
  ) { 

  }

  get() {
    return this.authService.schoolConnected$
    .pipe(
      switchMap(user => this.afs.collection<Message>('messages', ref => ref.where('schoolId','==',user.schoolId))
      .snapshotChanges()
      .pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data()
          const id = a.payload.doc.id
          return {id, ...data} as Message
        }))
      )
      )
    )
  }

  
  getById(id: string) {
    return this.authService.schoolConnected$
    .pipe(
      switchMap(user => this.afs.collection<Message>('messages', ref => ref.where('schoolId','==',user.schoolId).where('id','==',id))
      .snapshotChanges()
      .pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data()
          const id = a.payload.doc.id
          return {id, ...data} as Message
        }))
      )
      )
    )
  }

  add(data: Message) {
    return this.authService.schoolConnected$
    .pipe(
      switchMap(user => of(this.afs.collection<Message>('messages').add({...data, schoolId: user.schoolId} as Message))
      )
    )
  }

  get timestamp() {
    return firebase.firestore.FieldValue.serverTimestamp()
  }
}
