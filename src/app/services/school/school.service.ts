import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map, tap } from 'rxjs/operators';
import { School } from 'src/app/models/school/school';
import { Store } from 'src/app/shared/store/store';

@Injectable({
  providedIn: 'root'
})
export class SchoolService {

  schoolCollection: AngularFirestoreCollection

  constructor(
    private afs: AngularFirestore,
    private store: Store
  ) { 
    this.schoolCollection = this.afs.collection<School>('schools')
  }

  getById(id: string) {
    return this.schoolCollection
    .doc(id)
    .snapshotChanges()
    .pipe(
      map(a => {
        const data = a.payload.data() as Object;
        const schoolId = a.payload.id
        return {schoolId, ...data} as School;
      }),
      tap(school => this.store.set('schoolConnected',school))
    );

  }
}
