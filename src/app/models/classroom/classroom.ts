
export interface ClassRoom {
    id?: string;
    name: string;
    schoolId: string;
    createdAt?: firebase.firestore.FieldValue;
    updatedAt?: firebase.firestore.FieldValue;
}