
export interface Member {
    id?: string;
    name: string;
    lastName: string;
    classroom?: string[];
    number?: string;
    code?: string;
    schoolId: string;
    createdAt?: firebase.firestore.FieldValue;
    updatedAt?: firebase.firestore.FieldValue;
}