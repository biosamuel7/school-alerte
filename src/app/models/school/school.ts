
export interface School {
    schoolId: string;
    schoolName: string;
    schoolNumber: string;
    schoolEmail: string;
    createdAt: firebase.firestore.FieldValue;
    updatedAt: firebase.firestore.FieldValue;
}