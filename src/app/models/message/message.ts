import { Member } from '../members/member';

export interface Message {
    messageId?: string;
    messageBody: string;
    destinataires: Member[];
    schoolId?: string;
    createdAt?: firebase.firestore.FieldValue;
    updatedAt?: firebase.firestore.FieldValue;
}