import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'classroom',
  templateUrl: './classroom-infos.component.html',
  styleUrls: ['./classroom-infos.component.scss']
})
export class ClassroomInfosComponent implements OnInit {

  @Input() 
  classroom: string;
  
  constructor() { }

  ngOnInit(): void {
  }

}
