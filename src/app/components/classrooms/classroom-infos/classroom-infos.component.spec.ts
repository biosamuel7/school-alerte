import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassroomInfosComponent } from './classroom-infos.component';

describe('ClassroomInfosComponent', () => {
  let component: ClassroomInfosComponent;
  let fixture: ComponentFixture<ClassroomInfosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClassroomInfosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassroomInfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
