import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClassroomService } from 'src/app/services/classroom/classroom.service';

declare var $: any;

@Component({
  selector: 'add-classroom',
  templateUrl: './add-classroom.component.html',
  styleUrls: ['./add-classroom.component.scss']
})
export class AddClassroomComponent implements OnInit {

  @Output() 
  added: EventEmitter<String> = new EventEmitter<String>();

  classroomForm: FormGroup;

  get name () { return this.classroomForm.get('name')}

  constructor(
    private fb: FormBuilder,
    private service: ClassroomService,
  ) { }

  ngOnInit(): void {

    this.classroomForm = this.fb.group({
      name: ['', [
        Validators.required
      ]]
    })
    
  }

  onSave(){
    this.service.add({name: this.name.value}).subscribe(result => {
      this.added.emit(this.name.value)
      this.classroomForm.reset()
      $('#addClassroom').modal('toggle')
    })
    
  }

}
