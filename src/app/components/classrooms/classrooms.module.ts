import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClassroomsComponent } from './classrooms.component';
import { ClassroomInfosComponent } from './classroom-infos/classroom-infos.component';
import { RouterModule } from '@angular/router';
import { AddClassroomComponent } from './add-classroom/add-classroom.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ClassroomsComponent,
    ClassroomInfosComponent,
    AddClassroomComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule
  ]
})
export class ClassroomsModule { }
