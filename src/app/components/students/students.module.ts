import { CommonModule } from '@angular/common';
import { ElementRef, HostListener, Input, NgModule, OnInit } from '@angular/core';
import { StudentInfosComponent } from './student-infos/student-infos.component';
import { StudentsComponent } from './students.component';

@NgModule({
  declarations: [
    StudentInfosComponent,
    StudentsComponent
  ],
  imports: [
    CommonModule
  ]
})
export class StudentsModule { }
