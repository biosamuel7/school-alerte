import { Component, Input, OnInit } from '@angular/core';
import { Member } from 'src/app/models/members/member';

@Component({
  selector: 'student-infos',
  templateUrl: './student-infos.component.html',
  styleUrls: ['./student-infos.component.scss']
})
export class StudentInfosComponent implements OnInit {

  @Input() member: Member

  constructor() { }

  ngOnInit(): void {

  }

}
