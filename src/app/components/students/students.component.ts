import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Member } from 'src/app/models/members/member';
import { MemberService } from 'src/app/services/member/member.service';

declare function InitJS(): any;

@Component({
  selector: 'main-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent implements OnInit {

  datas$: Observable<Member[]>
  constructor(
    private service: MemberService
  ) { }

  ngOnInit(): void {
    InitJS();
    this.datas$ = this.service.get()
  }

}
