import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClassroomChooseComponent } from './step/classroom-choose/classroom-choose.component';
import { NumberAddComponent } from './step/number-add/number-add.component';
import { UserInfosComponent } from './step/user-infos/user-infos.component';
import { JoinComponent } from './join.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [ClassroomChooseComponent, NumberAddComponent, UserInfosComponent, JoinComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ]
})
export class JoinModule { }
