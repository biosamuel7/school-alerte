import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Member } from 'src/app/models/members/member';

@Component({
  selector: 'join-user-infos',
  templateUrl: './user-infos.component.html',
  styleUrls: ['./user-infos.component.scss']
})
export class UserInfosComponent implements OnInit {
  
  @Output()
  previous: EventEmitter<Number> = new EventEmitter<Number>()

  @Output()
  next: EventEmitter<Number> = new EventEmitter<Number>()

  @Output()
  infos: EventEmitter<Member> = new EventEmitter<Member>()

  @Input()
  step: number;

  form: FormGroup;

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {

    this.form = this.fb.group({
      name: ['',[
        Validators.required
      ]],
      lastName: ['',[
        Validators.required
      ]],
      birthDay: ['',[
        Validators.required
      ]],
      number: ['',[
        Validators.required
      ]]
    })

  }

  OnNext(){
    this.infos.emit(this.form.value as Member)
    this.next.emit(++this.step)
  }

  OnPrevious(){
    this.previous.emit(--this.step)
  }

  get name () { return this.form.get('name') }
  get lastName () { return this.form.get('lastName') }
  get birthDay () { return this.form.get('birthDay') }
  get number () { return this.form.get('number') }
}
