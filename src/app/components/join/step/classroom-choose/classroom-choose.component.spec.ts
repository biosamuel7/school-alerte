import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassroomChooseComponent } from './classroom-choose.component';

describe('ClassroomChooseComponent', () => {
  let component: ClassroomChooseComponent;
  let fixture: ComponentFixture<ClassroomChooseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClassroomChooseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassroomChooseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
