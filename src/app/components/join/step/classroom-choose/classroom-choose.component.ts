import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { ClassRoom } from 'src/app/models/classroom/classroom';
import { ClassroomService } from 'src/app/services/classroom/classroom.service';

@Component({
  selector: 'join-classroom-choose',
  templateUrl: './classroom-choose.component.html',
  styleUrls: ['./classroom-choose.component.scss']
})
export class ClassroomChooseComponent implements OnInit {

  @Output()
  previous: EventEmitter<Number> = new EventEmitter<Number>()

  @Output()
  next: EventEmitter<Number> = new EventEmitter<Number>()


  @Output()
  selected: EventEmitter<string> = new EventEmitter<string>()

  @Input()
  step: number;

  @Input()
  schoolId: string;

  datas$: Observable<ClassRoom[]>

  isSelected: boolean = false

  constructor(
    private service: ClassroomService
  ) { }

  ngOnInit(): void {
    this.datas$ = this.service.getBySchool(this.schoolId)
  }

  OnNext(){
    this.next.emit(++this.step)
  }

  OnPrevious(){
    this.previous.emit(this.step > 2 ? --this.step : this.step)
  }

  onChecked(check){
    this.isSelected = true
    this.selected.emit(check.value)
  }

}
