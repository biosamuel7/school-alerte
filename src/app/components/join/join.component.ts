import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { empty, iif, Observable, of } from 'rxjs';
import { switchMap, takeWhile, tap } from 'rxjs/operators';
import { Member } from 'src/app/models/members/member';
import { School } from 'src/app/models/school/school';
import { ClassroomService } from 'src/app/services/classroom/classroom.service';
import { MemberService } from 'src/app/services/member/member.service';
import { SchoolService } from 'src/app/services/school/school.service';

@Component({
  selector: 'main-join',
  templateUrl: './join.component.html',
  styleUrls: ['./join.component.scss']
})
export class JoinComponent implements OnInit {

  schoolConnected$: Observable<School>

  step: number = 1;

  member: Member;

  constructor(
    private route: ActivatedRoute,
    private service: SchoolService,
    private memberService: MemberService
  ) { }

  ngOnInit(): void {

    this.schoolConnected$ = this.route.queryParamMap.pipe(
      switchMap((params) => iif(() => !!params.get('school'), of(params.get('school')), of(undefined))),
      takeWhile(params => !!params),
      tap(i => console.log('res',i)),
      switchMap(params => this.service.getById(params))
      )
  }

  previous(step: number) {
    this.step = step
  }

  next(step: number){
    this.step = step
  }

  classRoom(classroomId: string) {
    this.member = {...this.member, classroom: [classroomId]}
    console.log(this.member)
  }

  userInfo(infos: Member) {
    this.member = {...this.member, ...infos}
    console.log(this.member)
  }

  userNumber(number: string){
    this.member = {...this.member, number}
    console.log(this.member)
  }

  finish(){
    this.route.queryParamMap.subscribe((params) => {
      this.memberService.add({...this.member, schoolId: params.get('school')}).then(result => {
        console.log(result)
      })
    })
    
  }


}
