import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MessagesRoutingModule } from './messages-routing.module';
import { WriteMessageComponent } from './write-message/write-message.component';
import { ListMessagesComponent } from './list-messages/list-messages.component';
import { MessagesComponent } from './messages.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [WriteMessageComponent, ListMessagesComponent, MessagesComponent],
  imports: [
    CommonModule,
    MessagesRoutingModule,
    ReactiveFormsModule
  ]
})
export class MessagesModule { }
