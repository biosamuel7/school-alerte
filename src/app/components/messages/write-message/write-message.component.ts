import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, from, forkJoin, of } from 'rxjs';
import { concat, concatAll, first, map, mergeMap, pluck, switchMap, tap, toArray } from 'rxjs/operators';
import { ClassRoom } from 'src/app/models/classroom/classroom';
import { Member } from 'src/app/models/members/member';
import { ClassroomService } from 'src/app/services/classroom/classroom.service';
import { MemberService } from 'src/app/services/member/member.service';
import { MessageService } from 'src/app/services/message/message.service';

declare var $: any;

@Component({
  selector: 'main-write-message',
  templateUrl: './write-message.component.html',
  styleUrls: ['./write-message.component.scss']
})
export class WriteMessageComponent implements OnInit {

  messageForm: FormGroup;

  classrooms$: Observable<ClassRoom[]>;

  constructor(
    private fb: FormBuilder,
    private classroomService: ClassroomService,
    private msgService: MessageService,
    private memberSerive: MemberService
  ) { }

  ngOnInit(): void {
    // initialization of select picker
    $('.js-custom-select').each(function () {
      // $.HSCore.components.HSSelect2.init($(this));
    });

    this.classrooms$ = this.classroomService.get().pipe(first())
    // this.classroomService.get().pipe(
    //   mergeMap(cls => {
    //     return of(cls['schoolId'])
    //   })
    // ).subscribe(d => console.log(d))

    this.messageForm = this.fb.group({
      messageBody: ['',[
        Validators.required
      ]],
      destinataires: ['', [
        Validators.required
      ]] 
    })
    
  }

  onSubmit() {
    this.memberSerive.getByClassroom(this.destinataires.value)
    .subscribe(result => {
      const dest = result.map(des => {
        return { number: des.number, lastName: des.lastName, name: des.name} as Member
      })
      console.log(dest)
      this.msgService.add({
        messageBody: this.messageBody.value,
        destinataires: dest
      }).subscribe(f => console.log(f))
    })

  }

  get messageBody () { return this.messageForm.get('messageBody') }
  get destinataires () { return this.messageForm.get('destinataires') }

}
