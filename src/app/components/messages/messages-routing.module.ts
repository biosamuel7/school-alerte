import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClassRoomResolver } from 'src/app/services/resolvers/classroom/class-room-resolver';
import { ListMessagesComponent } from './list-messages/list-messages.component';
import { MessagesComponent } from './messages.component';
import { WriteMessageComponent } from './write-message/write-message.component';

const routes: Routes = [{
  path: '', component: MessagesComponent,
  children: [
    { path: '', component: ListMessagesComponent},
    { 
      path: 'write', 
      component: WriteMessageComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessagesRoutingModule { }
