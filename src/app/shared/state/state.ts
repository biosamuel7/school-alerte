import { School } from 'src/app/models/school/school';

export interface State {
    school: School
}