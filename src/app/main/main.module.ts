import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { RouterModule } from '@angular/router';
import { StudentsModule } from '../components/students/students.module';
import { ClassroomsModule } from '../components/classrooms/classrooms.module';
import { ReactiveFormsModule } from '@angular/forms';
import { WrappersModule } from './wrappers/wrappers.module';
import { DashboardComponent } from '../components/dashboard/dashboard.component';


@NgModule({
  declarations: [
    MainComponent,
    DashboardComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    RouterModule,
    StudentsModule,
    ClassroomsModule,
    ReactiveFormsModule,
    WrappersModule
  ]
})
export class MainModule { }
