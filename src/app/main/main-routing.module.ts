import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClassroomsComponent } from '../components/classrooms/classrooms.component';
import { DashboardComponent } from '../components/dashboard/dashboard.component';
import { StudentsComponent } from '../components/students/students.component';
import { MainComponent } from './main.component';

const routes: Routes = [{
  path: '', component: MainComponent,
  children: [
    { path: '', redirectTo: 'dashboard', pathMatch: 'full'},
    { path: 'dashboard', component: DashboardComponent},
    { path: 'members', component: StudentsComponent},
    { path: 'messages', loadChildren: async() => (await import ('../components/messages/messages.module')).MessagesModule},
    { path: 'classrooms', component: ClassroomsComponent},
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
