import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JoinComponent } from './components/join/join.component';
import { AuthGuard } from './services/guards/auth/auth.guard';

const routes: Routes = [
  { 
    path: '', 
    loadChildren: async () =>  (await import('./main/main.module')).MainModule,
    canActivate: [AuthGuard]
  },
  { 
    path: 'auth', 
    loadChildren: async () => (await import('./components/auth/auth.module')).AuthModule
  },
  {
    path: 'join',
    component: JoinComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
