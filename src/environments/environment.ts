// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBiZ3c8s6djN-EkxfxgKFNbSoYX6Dz0Eyo",
    authDomain: "school-notification-28f5b.firebaseapp.com",
    databaseURL: "https://school-notification-28f5b.firebaseio.com",
    projectId: "school-notification-28f5b",
    storageBucket: "school-notification-28f5b.appspot.com",
    messagingSenderId: "182810064617",
    appId: "1:182810064617:web:7cfd225f17cc43ca132278",
    measurementId: "G-6QW5HVZ67J"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
